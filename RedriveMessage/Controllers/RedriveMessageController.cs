﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using RedriveMessage.Domain.Commands;
using System.Threading.Tasks;

namespace RedriveMessage.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RedriveMessageController : ControllerBase
    {

        [HttpPost]
        public async Task<IActionResult> RedriveMessages([FromServices] IMediator mediator, [FromBody] RedriveMessageRequest command)
        {
            var response = await mediator.Send(command);

            if (!response.IsSucess)
            {
                return BadRequest(response);
            }

            return Ok(response);
        }
    }
}
