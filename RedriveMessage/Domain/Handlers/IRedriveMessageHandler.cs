﻿using RedriveMessage.Domain.Commands;
using System.Threading.Tasks;

namespace RedriveMessage.Domain.Handlers
{
    public interface IRedriveMessageHandler
    {
        Task<RedriveMessageResponse> Handle(RedriveMessageRequest command);
    }
}