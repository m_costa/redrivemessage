﻿using Amazon.SQS.Model;
using MediatR;
using RedriveMessage.Domain.Commands;
using RedriveMessage.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RedriveMessage.Domain.Handlers
{
    public class RedriveMessageHandler : IRequestHandler<RedriveMessageRequest, RedriveMessageResponse>
    {
        private readonly IAWSSQSHelper _helper;

        public RedriveMessageHandler(IAWSSQSHelper helper)
        {
            _helper = helper;
        }

        public async Task<RedriveMessageResponse> Handle(RedriveMessageRequest request, CancellationToken cancellationToken)
        {
            var response = new RedriveMessageResponse();
            var messagesDeleteList = new List<Message>();
            var isHaveMessage = false;

            if (request.QueueUrlFrom.ToLower() == request.QueueUrlTo.ToLower())
            {
                response.IsSucess = false;
                response.Message = "A fila de origem de destino não pode ser iguais.";
                return response;
            }
            //else if (!request.QueueUrlFrom.ToLower().Contains("dlq"))
            //{
            //    response.IsSucess = false;
            //    response.Message = "Informe a url da fila dlq como origem";
            //    return response;
            //}
            else if (request.QueueUrlTo.ToLower().Contains("dlq"))
            {
                response.IsSucess = false;
                response.Message = "Não informe a url da fila dlq como destino";
                return response;
            }

            using (var client = _helper.SQSClient(request.QueueUrlFrom))
            {
                var messagesList = new List<Message>();

                do
                {
                    messagesList = await _helper.ReceiveMessageAsync(client, request.QueueUrlFrom);

                    if (messagesList.Any())
                    {
                        isHaveMessage = true;
                        foreach (var message in messagesList)
                        {
                            if (await _helper.SendMessageAsync(client, request.QueueUrlTo, message.Body))
                            {
                                messagesDeleteList.Add(message);
                            }
                        }

                        foreach (var deleteMessage in messagesDeleteList)
                        {
                            await _helper.DeleteMessageAsync(client, request.QueueUrlFrom, deleteMessage.ReceiptHandle);
                        }

                        messagesDeleteList.Clear();
                    }

                } while (messagesList.Any());
            }
            
            response.IsSucess = true;
            response.Message = isHaveMessage ? "As mensagens foram enviadas para a fila principal" : "Nenhuma mensagem encontrada na fila de origem";

            return response;
        }
    }
}
