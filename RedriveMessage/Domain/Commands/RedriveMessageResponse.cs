﻿namespace RedriveMessage.Domain.Commands
{
    public class RedriveMessageResponse
    {
        public bool IsSucess { get; set; }
        public string Message { get; set; }
    }
}
