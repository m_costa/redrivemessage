﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace RedriveMessage.Domain.Commands
{
    public class RedriveMessageRequest : IRequest<RedriveMessageResponse>
    {
        [Required]
        public string QueueUrlFrom { get; set; }

        [Required]
        public string QueueUrlTo { get; set; }
    }
}
