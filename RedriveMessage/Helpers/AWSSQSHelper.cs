﻿using Amazon;
using Amazon.SQS;
using Amazon.SQS.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedriveMessage.Helpers
{
    public interface IAWSSQSHelper
    {
        Task<bool> SendMessageAsync(AmazonSQSClient client, string queueUrlTo, string messageBody);
        Task<List<Message>> ReceiveMessageAsync(AmazonSQSClient client, string queueUrlFrom);
        Task<bool> DeleteMessageAsync(AmazonSQSClient client, string queueUrlFrom, string messageReceiptHandle);
        AmazonSQSClient SQSClient(string queueUrl);
    }

    public class AWSSQSHelper : IAWSSQSHelper
    {
        private readonly IConfiguration _configuration;

        public AWSSQSHelper(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<bool> SendMessageAsync(AmazonSQSClient client, string queueUrlTo, string messageBody)
        {
            try
            {
                var sendRequest = new SendMessageRequest(queueUrlTo, messageBody);
                // Post message or payload to queue  
                var sendResult = await client.SendMessageAsync(sendRequest);

                return sendResult.HttpStatusCode == System.Net.HttpStatusCode.OK;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<Message>> ReceiveMessageAsync(AmazonSQSClient client, string queueUrlFrom)
        {
            try
            {
                //Create New instance  
                var request = new ReceiveMessageRequest
                {
                    QueueUrl = queueUrlFrom,
                    MaxNumberOfMessages = 10,
                    WaitTimeSeconds = 5
                };

                //CheckIs there any new message available to process  
                var result = await client.ReceiveMessageAsync(request);

                return result.Messages.Any() ? result.Messages : new List<Message>();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> DeleteMessageAsync(AmazonSQSClient client, string queueUrlFrom, string messageReceiptHandle)
        {
            try
            {
                //Deletes the specified message from the specified queue  
                var deleteResult = await client.DeleteMessageAsync(queueUrlFrom, messageReceiptHandle);
                return deleteResult.HttpStatusCode == System.Net.HttpStatusCode.OK;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public AmazonSQSClient SQSClient(string queueUrl)
        {
            var accessKey = _configuration["AccessKey"];
            var secretKey = _configuration["SecretKey"];
            var localStackUrl = _configuration["LocalStackUrl"];
            var region = GetAwsRegion(queueUrl);

            if (!string.IsNullOrEmpty(localStackUrl))
            {
                return new AmazonSQSClient(new AmazonSQSConfig { ServiceURL = localStackUrl });
            }

            return new AmazonSQSClient(accessKey, secretKey, region);
        }

        private RegionEndpoint GetAwsRegion(string queueUrl)
        {
            if (queueUrl.Contains("sa-east"))
            {
                return RegionEndpoint.SAEast1;
            }

            return RegionEndpoint.USEast1;
        }
    }
}
